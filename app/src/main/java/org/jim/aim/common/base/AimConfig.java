package org.jim.aim.common.base;

import org.jim.aim.common.intel.ImHandler;

public class AimConfig {
    public ImHandler handler;
    public String ip;
    public int port;
    public int timeout=5000;
}
