package org.jim.aim.client.intel;

import org.jim.aim.common.base.ImPacket;

public interface AimMessageListener {
    void onConnected();
    void onRecieve(ImPacket packet);
}
