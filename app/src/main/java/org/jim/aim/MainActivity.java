package org.jim.aim;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.jim.aim.client.AimClient;
import org.jim.aim.client.intel.AimMessageListener;
import org.jim.aim.common.base.AimConfig;
import org.jim.aim.common.base.ImPacket;
import org.jim.aim.common.packets.ChatBody;
import org.jim.aim.common.packets.Command;
import org.jim.aim.common.packets.LoginReqBody;
import org.jim.aim.common.tcp.DefaultHandler;
import org.jim.aim.common.tcp.TcpPacket;

import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity {
    static AimClient client=AimClient.getInstance();
    TextView view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        view=(TextView) findViewById(R.id.test);
        AimConfig config=new AimConfig();
        Button button=(Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              new Thread(new Runnable() {
                  @Override
                  public void run() {
                      ChatBody chatBody = new ChatBody()
                              .setFrom("hello_client")
                              .setTo("1526529298615")
                              .setMsgType(0)
                              .setChatType(1)
                              .setGroup_id("100")
                              .setContent("android 客户端测试!");
                      client.sendMsg(chatBody);
                  }
              }).start();
            }
        });
        config.ip="192.168.13.223";
        config.port=8888;
        config.handler=new DefaultHandler();
        client.init(config);
        client.setListener(new AimMessageListener() {
            @Override
            public void onConnected() {
                byte[] loginBody = new LoginReqBody("hello_client","123").toByte();
                TcpPacket loginPacket = new TcpPacket(Command.COMMAND_LOGIN_REQ,loginBody);
                client.send(loginPacket);
            }

            @Override
            public void onRecieve(ImPacket packet) {
                Message msg=new Message();
                Bundle bund=new Bundle();
                try {
                    String str=new String(packet.getBody(),"utf-8");
                    System.out.println(str);
                    bund.putCharSequence("msg",str);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                msg.setData(bund);
                handler.sendMessage(msg);
            }
        });
        client.connect();
    }
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            view.setText(msg.getData().getCharSequence("msg"));
        }
    };
}
