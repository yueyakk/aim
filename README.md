# aim

#### 项目介绍
对接j-im服务端的一个android demo

#### 
由于android没有aio,所以使用普通的java socket接口编写的连接j-im服务端的android demo.其实在普通的java程序中也是可以运行的，发送与接受数据的结构与j-im client demo 差不多

#### 使用说明

1. 创建配置
```
       AimClient client=AimClient.getInstance();
        config.ip="192.168.13.223";
        config.port=8888;
        config.handler=new DefaultHandler(); 
```
2. 初始化
```
    client.init(config);
```
3. 设置监听对象
```
    client.setListener(new AimMessageListener() {
            @Override
            public void onConnected() {
              //在这里可以发送登陆请求
            }

            @Override
            public void onRecieve(ImPacket packet) {
              //接收信息
            }
        });
```
4 连接 
```
    client.connect();
    cilent.sendMsg(ChatBody chat)
```
5 发送信息
```
    cilent.sendMsg(ChatBody chat)
```